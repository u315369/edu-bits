import time
from netmiko import Netmiko
import json
import re

start = time.time()

device_dict = {
    "host": "192.168.1.110",
    "username": "admin",
    "password": "Cisco",
    "device_type": "cisco_ios",
    "ssh_config_file": "./proxy_ssh_cfg"
}

intf_lst_ios = [
    "Ethernet0/0 is up, line protocol is up ",
    "Hardware is AmdP2, address is aabb.cc00.0100 (bia aabb.cc00.0100)",
    "Internet address is 192.168.1.120/24",
    "MTU 1500 bytes, BW 10000 Kbit/sec, DLY 1000 usec, ",
    "reliability 255/255, txload 1/255, rxload 1/255",
    "Ethernet1/2 is administratively down, line protocol is down \n",
    "Hardware is AmdP2, address is aabb.cc00.0121 (bia aabb.cc00.0121)\n",
    "MTU 1500 bytes, BW 10000 Kbit/sec, DLY 1000 usec, \n",
    "reliability 255/255, txload 1/255, rxload 1/255\n",
    "Encapsulation ARPA, loopback not set\n",
    "Keepalive set (10 sec)\n",
    "ARP type: ARPA, ARP Timeout 04:00:00\n",
    "Last input never, output never, output hang never\n",
    "Last clearing of \"show interface\" counters never\n",
    "Input queue: 0/75/0/0 (size/max/drops/flushes); Total output drops: 0\n",
    "Queueing strategy: fifo\n",
    "Output queue: 0/40 (size/max)\n",
    "5 minute input rate 0 bits/sec, 0 packets/sec\n",
    "5 minute output rate 0 bits/sec, 0 packets/sec\n",
    "Ethernet0/0 is administratively down, line protocol is down \n",
    "Hardware is AmdP2, address is aabb.cc00.0131 (bia aabb.cc00.0131)\n",
    "MTU 1500 bytes, BW 10000 Kbit/sec, DLY 1000 usec, \n",
    "reliability 255/255, txload 1/255, rxload 1/255\n",
    "Ethernet0/1 is administratively down, line protocol is down \n",
    "Hardware is AmdP2, address is aabb.cc00.0131 (bia aabb.cc00.0131)\n",
    "MTU 1500 bytes, BW 10000 Kbit/sec, DLY 1000 usec, \n",
    "reliability 255/255, txload 1/255, rxload 1/255\n",
    "Ethernet1/3 is administratively down, line protocol is down \n",
    "Hardware is AmdP2, address is aabb.cc00.0131 (bia aabb.cc00.0131)\n",
    "MTU 1500 bytes, BW 10000 Kbit/sec, DLY 1000 usec, \n",
    "reliability 255/255, txload 1/255, rxload 1/255\n",
    "Encapsulation ARPA, loopback not set\n",
    "Keepalive set (10 sec)\n",
    "ARP type: ARPA, ARP Timeout 04:00:00\n",
    "Last input never, output never, output hang never\n",
    "Last clearing of \"show interface\" counters never\n",
    "Input queue: 0/75/0/0 (size/max/drops/flushes); Total output drops: 0\n",
    "Queueing strategy: fifo\n",
    "Output queue: 0/40 (size/max)\n",
    "5 minute input rate 0 bits/sec, 0 packets/sec\n",
    "5 minute output rate 0 bits/sec, 0 packets/sec\n",
    "0 packets input, 0 bytes, 0 no buffer\n",
    "Received 0 broadcasts (0 IP multicasts)\n",
    "0 runts, 0 giants, 0 throttles \n",
    "0 input errors, 0 CRC, 0 frame, 0 overrun, 0 ignored\n",
    "0 input packets with dribble condition detected\n",
    "0 packets output, 0 bytes, 0 underruns\n",
    "0 output errors, 0 collisions, 0 interface resets\n",
    "0 unknown protocol drops\n",
    "0 babbles, 0 late collision, 0 deferred\n",
    "0 lost carrier, 0 no carrier\n",
    "0 output buffer failures, 0 output buffers swapped out",
]

intf_lst_eos = [
    "Ethernet1 is up, line protocol is up (connected)",
    "  Hardware is Ethernet, address is 0c33.a55c.be9b",
    "  Description: *** This is the example of a wordy note ***",
    "  Internet address is 10.10.10.110/24",
    "  Broadcast address is 255.255.255.255",
    "  Address determined by manual configuration",
    "  IP MTU 1500 bytes",
    "  Full-duplex, Unconfigured, auto negotiation: off, uni-link: n/a",
    "  Up 3 days, 1 hours, 25 minutes, 1 seconds",
    "  Loopback Mode : None",
    "  1 link status changes since last clear",
    "  5 minutes input rate 0 bps (- with framing overhead), 0 packets/sec",
    "  5 minutes output rate 0 bps (- with framing overhead), 0 packets/sec",
    "     0 packets input, 0 bytes",
    "     Received 0 broadcasts, 0 multicast",
    "     0 runts, 0 giants",
    "     0 input errors, 0 CRC, 0 alignment, 0 symbol, 0 input discards",
    "     0 PAUSE input",
    "     8854 packets output, 1512311 bytes",
    "     Sent 0 broadcasts, 8854 multicast",
    "     0 output errors, 0 collisions",
    "     0 late collision, 0 deferred, 0 output discards",
    "     0 PAUSE output",
    "Ethernet12 is up, line protocol is up (connected)",
    "  Hardware is Ethernet, address is 0c33.a5f8.660c (bia 0c33.a5f8.660c)",
    "  Ethernet MTU 9214 bytes",
    "  Full-duplex, Unconfigured, auto negotiation: off, uni-link: n/a",
    "  Up 3 days, 1 hours, 25 minutes",
    "  Loopback Mode : None",
    "  1 link status changes since last clear",
    "  5 minutes input rate 0 bps (- with framing overhead), 0 packets/sec",
    "  5 minutes output rate 0 bps (- with framing overhead), 0 packets/sec",
    "     0 packets input, 0 bytes",
    "     Received 0 broadcasts, 0 multicast",
    "     0 runts, 0 giants",
    "     0 input errors, 0 CRC, 0 alignment, 0 symbol, 0 input discards",
    "     0 PAUSE input",
    "     140603 packets output, 17725977 bytes",
    "     Sent 0 broadcasts, 140603 multicast",
    "     0 output errors, 0 collisions",
    "     0 late collision, 0 deferred, 0 output discards",
    "     0 PAUSE output",
    "Management1 is up, line protocol is up (connected)",
    "  Hardware is Ethernet, address is 0c33.a5f8.6600 (bia 0c33.a5f8.6600)",
    "  Description: *** Mgmt Interface - connection to IOU switch ***",
    "  Internet address is 192.168.1.110/24",
    "  Broadcast address is 255.255.255.255",
    "  Address determined by manual configuration",
    "  IP MTU 1500 bytes , BW 1000000 kbit",
    "  Full-duplex, 1Gb/s, auto negotiation: on, uni-link: n/a",
    "  Up 3 days, 1 hours, 23 minutes, 34 seconds",
    "  Loopback Mode : None",
    "  5 link status changes since last clear",
    "  5 minutes input rate 2.09 kbps (0.0% with framing overhead), 3 packets/sec",
    "  5 minutes output rate 4.33 kbps (0.0% with framing overhead), 3 packets/sec",
    "     9029 packets input, 671268 bytes",
    "     Received 17 broadcasts, 0 multicast",
    "     0 runts, 0 giants",
    "     0 input errors, 0 CRC, 0 alignment, 0 symbol, 0 input discards",
    "     0 PAUSE input",
    "     26436 packets output, 2784698 bytes",
    "     Sent 5 broadcasts, 8812 multicast",
    "     0 output errors, 0 collisions",
    "     0 late collision, 0 deferred, 0 output discards",
    "     0 PAUSE output",
]


cmd = 'show interfaces'
intf_dict = {}
MAC_RE = re.compile("[a-fA-F0-9]{4}\.[a-fA-F0-9]{4}\.[a-fA-F0-9]{4}")
DESC_RE = re.compile("^\s+Description:\s+(.+)$")
ETH_RE = re.compile("^(\S+\d+)\s+is\s+(\w+)")
MTU_RE = re.compile("^(.+)\s+MTU\s+(\d+)")
IP_RE = re.compile(
    "^\s+Internet\s+address\s+is\s+(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/\d+)")

interface_regex_1 = r"^(\S+?)\s+is\s+(.+?),\s+line\s+protocol\s+is\s+(\S+)"
interface_regex_2 = r"^(\S+)\s+is\s+(up|down)"

# connect to device and run command
print('\nLogging into device {}\n'.format(device_dict['host']))
nc = Netmiko(**device_dict)
print('\n  Running Command : {}\n'.format(cmd))
output = nc.send_command(cmd)
output_lst = output.splitlines()

print("==" * 30)

ip = False
for line in output_lst:
    # for line in intf_lst_eos:
    if re.search(ETH_RE, line):
        intf_name = re.search(ETH_RE, line).group(1)
        intf_dict[intf_name] = {}

        if "admin" in re.search(ETH_RE, line).group(2).lower():
            intf_status = 'down'
            intf_enable = False
        else:
            intf_status = 'up'
            intf_enable = True
        intf_dict[intf_name]['status'] = intf_status
        intf_dict[intf_name]['enable'] = intf_enable
        intf_dict[intf_name]['description'] = False
        intf_dict[intf_name]['mac_address'] = False

    if re.search(DESC_RE, line):
        description = re.search(DESC_RE, line).group(1)
        intf_dict[intf_name]['description'] = description

    if re.search(MAC_RE, line):
        mac_address = re.search(MAC_RE, line).group()
        intf_dict[intf_name]['mac_address'] = mac_address

    if re.search(IP_RE, line):
        ip = re.search(IP_RE, line).group(1)

    if re.search(MTU_RE, line):
        mtu_type = re.search(MTU_RE, line).group(1).strip()
        mtu_size = re.search(MTU_RE, line).group(2)
        intf_dict[intf_name]['mtu'] = '{} ({})'.format(mtu_size, mtu_type)

        if 'IP' in mtu_type:
            intf_dict[intf_name]['IP'] = ip

print(json.dumps(intf_dict, indent=4))
print("runtime: {}".format(time.time() - start))
