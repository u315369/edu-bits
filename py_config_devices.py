from netmiko import ConnectHandler
import telnetlib
import time
from myNetMods import get_mode, get_in, iou_rtr1
import logging


def go_ssh():
    """ """
    print("Attempting SSH . . . ")
    my_device = {
        'host': '192.168.1.130',
        'username': 'admin',
        'password': 'Cisco',
        'device_type': 'cisco_ios',
    }

    nc = ConnectHandler(**my_device)
    return nc


# logging.basicConfig(filename='test.log', level=logging.DEBUG)
# logger = logging.getLogger("netmiko")

start = time.time()
host = '192.168.1.150'
port = '23'
dev_type = None

# Access device to determine device device_type.
try:
    tn = telnetlib.Telnet(host, port, timeout=2)
    # tn.set_debuglevel(1)
    # print(tn.get_socket())
except Exception as e:
    raise SystemExit(
        '\nERROR: Telnet failed to {} on port 23, {}'.format(host, e))

device_state = get_mode(tn)[0]

if 'user' in device_state:
    # login into devices
    login = get_in(tn, "admin", "Cisco")
    if login:
        tn.write('sh ver\n'.encode())
        r = tn.read_until('>'.encode(), timeout=2)
        if r.decode().find('Arista') >= 1:
            dev_type = 'arista_eos'
        else:
            dev_type = 'cisco_ios'
    else:
        raise SystemExit('\nERROR: Could not login to device.', login)
else:
    print(device_state)
# tn.close()
print("Device Type: ", dev_type)

my_device = {
    'host': host,
    'username': 'admin',
    'password': 'Cisco',
    'device_type': dev_type,
}

# SSH connection to device
try:
    nc = ConnectHandler(**my_device)
except Exception as e:
    logging.error(e)
    print('Attempting to config', host)
    ret_stat = iou_rtr1(tn, hostname='R11')
    print('return code >>', ret_stat)
    if not ret_stat:
        quit()
    nc = ConnectHandler(**my_device)

if not nc:
    quit()

prompt = nc.find_prompt()
# Print the prompt/hostname of the device
print("Prompt: {}".format(prompt))
print("runtime: {0:.2f}".format(time.time() - start))
quit()

# Ensure we are in enable mode and can make changes.
if "#" not in prompt[-1]:
    print("  - Entering enable mode")
    nc.enable()

try:
    op = nc.send_config_from_file('commands_file')
    print(op)
    nc.disconnect()
except Exception as e:
    print(e)
    raise SystemExit(e)

print("runtime: {0:.2f}".format(time.time() - start))
