import telnetlib

port = '5015'
prompt = "ASW-2#"

# check if ssh service is enabled and ping connectivity
try:
    tn = telnetlib.Telnet('192.168.0.21', port, timeout=2)
except Exception as e:
    raise SystemExit(
        '\nERROR: Telnet failed to port {}, {}'.format(port, e))

for x in range(10, 50, 2):
    cmd = 'clear line vty {}'.format(x)
    print(cmd)
    tn.write(cmd.encode() + '\n'.encode())
    tn.read_until('comfirm'.encode(), timeout=2)
    tn.write('\n'.encode() + '\n\n\n'.encode())
    r = tn.read_until(prompt.encode(), timeout=2)
    print(r)

'''
tn.write('ip add 192.168.1.120'.encode() +
                tn55.255.0\n'.encode())
tn.write('no shut'.encode() + '\n\n'.encode())
tn.write('exit'.encode() + '\n\n'.encode())
tn.read_until(prompt.encode(), timeout=5)

line_output = {}
host = my_device_dict['host']
line_output[my_device_dict['host']] = output
net_connect.enable()  # Ensure in enable mode
vty_data = normalizeData(li
for line in vty_data:
    if 'vty' in line.decode('ascii'):
        if '*' not in line.decode('ascii'):
            cmd = 'clear line vty {}'.format(
                line.decode('ascii').split()[2])
            print('clearing: ', cmd)
            d = net_connect.send_command(
                cmd, expect_string='onfirm')
            print(d)
            net_connect.send_command('\n')
'''
