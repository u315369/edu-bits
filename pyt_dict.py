'''
Purpose:
    In this script we will learn how to add or append new key
    value pairs in a dictionary and also how to update value of existing keys.
'''


# Define the dictionary
dict = {
    'ints': [],
    'arp': [],
    'routes': [],
    'who': '',
    'new_dict': {},
}

'''
dict = {
    'ints': [],         ==> vals = type list : use append
    'arp': [],          ==> vals = type list : use append
    'routes': [],       ==> vals = type list : use append
    'who': '',          ==> vals = type string : use assignment (better to use a list w/ append)
    'int_brief': {},    ==> vals = type dict : use update
}
'''

# Additional Q's:
#   1. What are the attributes to modify a STRING within a DICT?
#   2. Test the rules of Try, except clause.


def various():

    results = {}

    # Append values to dictionary 'list' keys
    try:
        dict['ints'].append(inter_blank)
        dict['ints'].append(interface)
        dict['ints'].append('Test STRING')
    except Exception as e:
        results['Try 1'] = ': Append error using val as {}: {}'.format(type(dict['ints']), e)

    # Append value to dictionary 'string' key
    try:
        dict['who'].append('2 vty 5 admin, append')
        print('Try 2')
    except Exception as e:
        results['Try 2'] = ': Append error using val as {}: {}'.format(type(dict['who']), e)

    # Update value to dictionary 'string' key
    try:
        dict['who'].update('2 vty 5 admin, update')
    except Exception as e:
        results['Try 3'] = ': Update error using val as {}: {}'.format(type(dict['who']), e)

    # Assign value to dictionary 'string' key
    try:
        dict['who'] = 'This is the 1st string, '
        dict['who'] += 'This is the 2nd string'
    except Exception as e:
        results['Try 4'] = ': Assignment error using val as {}: {}'.format(type(dict['who']), e)

    # Assign value to dictionary 'string' key
    try:
        dict['who'].update(arp)
    except Exception as e:
        results['Try 5'] = ': Update data error using val as {}: {}'.format(type(dict['who']), e)

    # Update value to dictionary 'list' key
    try:
        dict['arp'].update(arp)
    except Exception as e:
        results['Try 6'] = ': Update error using val as {}: {}'.format(type(dict['arp']), e)

    # Update value to dictionary 'list' key
    try:
        dict['new_dict'].update(arp_dict)
    except Exception as e:
        results['Try 7'] = ': Update error using val as {}: {}'.format(type(dict['new_dict']), e)
    print()

    # Addend value to dictionary 'list' key
    try:
        dict['arp'].append(arp)
    except Exception as e:
        results['Try 8'] = ': Append error using val as {}: {}'.format(type(dict['arp']), e)

    return results


# Define the static lists
interface = ['fa0/0', 'fa0/1', 'fa1/0', 'fa1/1']
inter_blank = []
arp = ['192.168.1.2, N/A, 866a.1be7.98e1, Management1']
arp_dict = {'key1': 'THIS IS THE NEW DICT TYPE ADDED!!!!!!'}
routes = ['C, 10.10.10.0/24 is directly connected, Ethernet1']
multiline = [
    'Interface              IP Address         Status     Protocol         MTU\n'
    'Network011             10.10.10.110/24    up         up              1500\n'
    'Network021             192.168.1.110/24   up         up              1500'
]

ret = various()

# display GOOD dict items
for k, v in dict.items():
    print('{}{:<3} {} :: {}'.format(type(v), '', k, v))

# display exceptions
if ret:
    print("\n{:#<70}\n\n Errors\n".format(''))
    for k, v in ret.items():
        print('{:<2}{} {}'.format('', k, v))

'''
# dynamically add to dictionary no. 1
print("\n{:#<70}\n\n Dynamic Additions".format(''))
dyn = {}
for line in multiline:
    try:
        dyn['in1_brief'] = line
    except Exception as e:
        print('Update Error:', e)
'''

# dynamically add to dictionary no. 2
print("\n{:#<70}\n\n Dynamic Additions".format(''))
dyn = {'multi-line': []}
for line in multiline:
    try:
        # dyn['multi-line'] = line
        dyn['multi-line'].append(line)
    except Exception as e:
        print('Update Error:', e)

# display dynamic dictionary
print('\n Num of entries:', len(dyn), '\n')
for k, v in dyn.items():
    print(' - Key Type: {:<2}{}'.format('', type(k)))
    print(' - Key: {:<7}{}'.format('', k))
    print(' - Val Type: {:<2}{}'.format('', type(v)))
    if len(v) > 40:
        print(' - Value:')
        print(v)
    else:
        print(' - Value: {:<5}{}'.format('', v))
print('\n')

# display RAW dyn dictionary
print(dyn)
