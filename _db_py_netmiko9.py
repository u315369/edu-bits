from netmiko import ConnectHandler

with open('commands_file') as f:
    commandstosend = f.read().splitlines()

print(commandstosend)
print()

iosv_devices = {
    'device_type': 'cisco_ios',
    'ip': '192.168.1.130',
    'username': 'admin',
    'password': 'Cisco',
}

all_devices = [iosv_devices]

for devices in all_devices:
    net_connect = ConnectHandler(**devices)
    output2 = net_connect.send_config_set(commandstosend)
print(output2)

"""
for item_cmd in commandstosend:
	print("Send cmd: ", item_cmd)
	output2 = net_connect.send_command(item_cmd)
	print(output2)
"""
