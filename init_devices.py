from netmiko import ConnectHandler
import telnetlib
import time
from myNetMods import get_mode, get_in, get_in_confmode
import logging
import sys
import os
import json
from pprint import pprint as pp


def get_devType(tn):
    """DOCSTRING"""

    tn.write('sh ver\n\n'.encode())
    r = tn.read_until('IOS'.encode(), timeout=2)

    if r.decode().find('Arista') >= 1:
        return ('arista_eos')

    return ('cisco_ios')


def init_conf(filename, path="./"):
    """Open and read JSON configuration files """

    try:
        with open(os.path.join(path, filename)) as file:
            jdata = json.load(file)
    except IOError:
        sys.exit(
            f"[HALT]: Unable to open or locate configuration file : {filename}")

    return jdata


def go_ssh():
    """ """
    print("Attempting SSH . . . ")
    my_device = {
        'host': '192.168.1.130',
        'username': 'admin',
        'password': 'Cisco',
        'device_type': 'cisco_ios',
    }

    '''
    SSH command to old Cisco
        ssh -oKexAlgorithms=+diffie-hellman-group1-sha1 -c aes128-cbc admin@192.168.122.x
    '''
    nc = ConnectHandler(**my_device)
    return nc


def cfgDevice(tnobj, cfg, dev_type=None):
    """ DOCSTRING """

    # configure oob mgt on all device
    if get_in_confmode(tn):
        for cmd in cfg:
            cmd = f"{cmd}\n\r".encode()
            tnobj.write(cmd)
            tnobj.read_until(')#'.encode(), timeout=5)

    if dev_type:
        if 'ios' in dev_type:
            print(f" DEVICE TYPE :: {dev_type} : Running Modules CFG")
            cfgModules(tnobj)


def cfgModules(tnobj):
    # setup ssh configuration
    feedback = [b"modulus [512]:", b"replace them"]
    tnobj.write('crypto key generate rsa'.encode() + '\r'.encode())
    _idx, _obj, _match = tnobj.expect(feedback, timeout=5)
    if _idx == 1:
        print(" ** RSA keys already defined. Replacing")
        tnobj.write('yes'.encode() + '\r'.encode())
        print(" ** Entering module size of 1024")
        tnobj.write('1024'.encode() + '\r'.encode())
        tnobj.read_until(')#'.encode(), timeout=5)
        tnobj.write('end'.encode() + '\r'.encode())
    return

# logging.basicConfig(filename='test.log', level=logging.DEBUG)
# logger = logging.getLogger("netmiko")


start = time.time()
host = 'localhost'

# read in devices from json file
device_details = init_conf('init_dev_conf.json')
pp(device_details)

# access each device
for port in device_details['devices'].keys():
    if device_details['devices'][port]['install'] == 'yes':

        dev_type = device_details['devices'][port]['type']
        dev_mgmt = device_details['devices'][port]['mgmtIP']
        dev_name = device_details['devices'][port]['hostname']
        dev_conf = device_details['devices'][port]['preinstall']

        # check if ssh is already configured
        ssh_devices = {}
        try:
            print(f"\nChecking ssh on: {dev_mgmt}")
            telnetlib.Telnet(dev_mgmt, '22', timeout=3)
            print(f" -- GOOD, ssh configured: {dev_mgmt}")
            ssh_devices[port]['host'] = dev_mgmt
            ssh_devices[port]['type'] = dev_type
            continue
        except:
            print(f" -- ssh and mgt conf required")

        # console to device as needed
        try:
            tn = telnetlib.Telnet(host, port, timeout=2)
        except Exception as e:
            print(f' -- ERROR: Telnet failed to {host} on port {port}, {e}')
        else:
            # determine device status/mode
            device_state, dev_prompt = get_mode(tn)

            # login routine for devices
            if device_state:
                enbl_prompt = f"{dev_name}#"
                login_status = 'Not Required'
                verify = None
                print("--" * 30)
                print(f"Name: {dev_name}")
                print(f"Type: {dev_type}")
                print(f"Port: {port}")
                print(f"Mode: {device_state}")
                print(f"IP:   {dev_mgmt}")

                # determine if login required, if so login
                if 'priv' not in device_state and 'config' not in device_state:
                    # Arista login
                    if 'user' in device_state:
                        login_status = 'Arista login require...attempting'
                        get_in(tn, "admin", "Cisco")

                    # FRRouter login
                    if 'login' in device_state and 'frr' in dev_prompt:
                        login_status = 'FRRouter login require...attempting'
                        get_in(tn, "frradmin", "frr")

                    # verify access was granted
                    verify = 'login good' if 'priv' in get_mode(
                        tn)[0] else 'login failed'

                print(f"Login: {login_status}")
                if verify:
                    print(f"Verification: {verify}")
                print(f"enable prompt: {enbl_prompt}")
                print(f"device prompt: {dev_prompt}")
                print()

            if dev_conf:
                r = cfgDevice(tn, dev_conf) if 'ios' not in dev_type else \
                    cfgDevice(tn, dev_conf, dev_type)

pp(ssh_devices)
quit()


my_device = {
    'host': host,
    'username': 'admin',
    'password': 'Cisco',
    'device_type': dev_type,
}

# SSH connection to device
try:
    nc = ConnectHandler(**my_device)
except Exception as e:
    logging.error(e)
    print('Attempting to config', host)
    ret_stat = iou_rtr1(tn, hostname='R11')
    print('return code >>', ret_stat)
    if not ret_stat:
        quit()
    nc = ConnectHandler(**my_device)

if not nc:
    quit()

prompt = nc.find_prompt()
# Print the prompt/hostname of the device
print("Prompt: {}".format(prompt))
print("runtime: {0:.2f}".format(time.time() - start))
quit()

# Ensure we are in enable mode and can make changes.
if "#" not in prompt[-1]:
    print("  - Entering enable mode")
    nc.enable()

try:
    op = nc.send_config_from_file('commands_file')
    print(op)
    nc.disconnect()
except Exception as e:
    print(e)
    raise SystemExit(e)

print("runtime: {0:.2f}".format(time.time() - start))
