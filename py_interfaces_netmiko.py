import json
import re
import time
from netmiko import Netmiko
from pprint import pprint as pp

start = time.time()

devices_list = ["192.168.1.110", "192.168.1.100"]
device_dict = {
    "host": "",
    "username": "admin",
    "password": "Cisco",
    "device_type": "cisco_ios",
    "ssh_config_file": "./proxy_ssh_cfg"
}


cmd = 'show interface '
intf_dict = {}
intf = {}

MAC_RE = re.compile("[a-fA-F0-9]{4}\.[a-fA-F0-9]{4}\.[a-fA-F0-9]{4}")
DESC_RE = re.compile("^\s+Description:\s+(.+)$")
ETH_RE = re.compile("^(\S+\d+)\s+is\s+(\w+)")
MTU_RE = re.compile("^(.+)\s+MTU\s+(\d+)")
IP_RE = re.compile(
    "^\s+Internet\s+address\s+is\s+(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/\d+)")

# part 1 - connect and run commands
for device in devices_list:
    device_dict['host'] = device
    try:
        print('\n Connecting to host {}'.format(device))
        nc = Netmiko(**device_dict)
    except Exception as e:
        raise SystemExit('\nERROR: SSH failed to {}, {}'.format(device, e))

    if nc:
        host_name = device_dict['host']
        intf_dict[host_name] = {}

        print('  -- running command : {}\n'.format(cmd))
        output = nc.send_command(cmd)
        int_list = output.splitlines()

        # part 2 - parse the output
        ip = False

        for line in int_list:
            if re.search(ETH_RE, line):
                intf_name = re.search(ETH_RE, line).group(1)
                intf_dict[host_name][intf_name] = {}

                if "admin" in re.search(ETH_RE, line).group(2).lower():
                    intf_status = 'down'
                    intf_enable = False
                else:
                    intf_status = 'up'
                    intf_enable = True
                intf_dict[host_name][intf_name]['status'] = intf_status
                intf_dict[host_name][intf_name]['enable'] = intf_enable
                intf_dict[host_name][intf_name]['description'] = False
                intf_dict[host_name][intf_name]['mac_address'] = False

            if re.search(DESC_RE, line):
                description = re.search(DESC_RE, line).group(1)
                intf_dict[host_name][intf_name]['description'] = description

            if re.search(MAC_RE, line):
                mac_address = re.search(MAC_RE, line).group()
                intf_dict[host_name][intf_name]['mac_address'] = mac_address

            if re.search(IP_RE, line):
                ip = re.search(IP_RE, line).group(1)

            if re.search(MTU_RE, line):
                mtu_type = re.search(MTU_RE, line).group(1).strip()
                mtu_size = re.search(MTU_RE, line).group(2)
                intf_dict[host_name][intf_name]['mtu'] = '{} ({})'.format(
                    mtu_size, mtu_type)

                if 'IP' in mtu_type:
                    intf_dict[host_name][intf_name]['IP'] = ip

end = time.time()
print(json.dumps(intf_dict, indent=4))
print("\nruntime: {}".format(end - start))


'''
{
    "Ethernet0/0": {
        "description": "",
        "is_enabled": true,
        "is_up": true,
        "last_flapped": -1.0,
        "mac_address": "AA:BB:CC:00:01:00",
        "mtu": 1500,
        "speed": 10
    },
    "Ethernet0/1": {
        "description": "",
        "is_enabled": false,
        "is_up": false,
        "last_flapped": -1.0,
        "mac_address": "AA:BB:CC:00:01:10",
        "mtu": 1500,
        "speed": 10
    },
'''
