'''
Define a class named EggCarton in a file named EggCarton.py. EggCartons hold
a number of eggs, have a 'take' method, and have a 'look' method.
A new EggCarton always starts with 12 eggs. The instance method 'take'
only takes in the required self as a parameter. If the number of eggs in
the carton is greater than 0, the take method reduces the number of eggs by
1 and returns True. Otherwise the 'take' method returns False. The 'look'
method returns the number of eggs remaining in the EggCarton.

Add my sample program code after your EggCarton class definition in your
EggCarton.py file. When run it should generate output identical to
my sample output.

Sample output from my program:
Carton 1 contains 12 eggs
taking 3 eggs
Carton 1 contains 9 eggs
Carton 2 contains 12 eggs
'''


class EggCarton:

    def __init__(self, amount=12):
        self.amount = amount

    def take(self):
        if self.amount > 0:
            self.amount -= 1
            return True
        return False

    def look(self):
        return self.amount

        # A recursive function that empties an egg carton


def empty(cart):
    if cart.take():
        # An egg was successfully taken, take another one
        print(".")
        empty(cart)
    else:
        # All out of eggs
        print("The carton is now empty")


# Make the cartons
cart1 = EggCarton()
cart2 = EggCarton()

# Work with the first carton a little bit
print("Carton 1 contains " + str(cart1.look()) + " eggs")
print("taking 3 eggs")
cart1.take()
cart1.take()
cart1.take()
print("Carton 1 contains " + str(cart1.look()) + " eggs")

# Work with the second carton a little bit
print("Carton 2 contains " + str(cart2.look()) + " eggs")
empty(cart2)
print("Carton 2 contains " + str(cart2.look()) + " eggs")
