from netmiko import Netmiko

dev_type = 'cisco_ios'
host = '192.168.1.150'

my_device = {
    'host': host,
    'username': 'admin',
    'password': 'Cisco',
    'device_type': dev_type,
}

# SSH connection to device
try:
    nc = Netmiko(**my_device)
except Exception as e:
    raise SystemExit(e)
