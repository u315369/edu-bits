from pprint import pprint as pp
import os

'''
# prompt user for information
name = input("Please enter person name: ")
year = input("Please enter year: ")

# construct filename
filename = 'yob' + year + '.txt'

# storing records in list
records = []

# check if filename exists
if os.path.isfile('us_names/' + filename):
    print(f" \nSearching {filename}")

    # open file and search for user inputted name
    with open('us_names/' + filename) as fh:
        for line in fh:
            n, s, x = line.split(',')
            if name.title() == n:
                records.append(line.strip())
                print('\nLocated Information:')
                print(f" - Name: {n}")
                print(f" - Sex:  {s}")
                print(f" - Num:  {x}")
else:
    print(f' \nFile can not be locate, {filename}')

if not records:
    print(f"{name.title()} is not used in the year {year}")

print(records)
print(f"{' * ' * 25}")
'''

# --------------- R A N G E  of  F I L E S ----------------


def maximum_keys(d):
    names_list = []
    max_value = max(d.values())

    # getting all keys containing the `maximum`
    for name, val in d.items():
        if val == max_value:
            names_list.append(name)

    return max_value, names_list


filenames = []
records = {}
startyr = int(input("Enter your start year:  "))
endyr = int(input("Enter your ending year:  "))
sex = input("Enter sex (M/F):  ").upper()

# validate sex entry
if sex not in ['M', 'F']:
    exit('Please enter M for male or F for female')

# range of files must be inclusive
print(f"\ndifference in years: {endyr - startyr}")
for yr in range(startyr, endyr + 1):
    filename = 'yob' + str(yr) + '.txt'
    filenames.append(filename)

# open file and search for user inputted name
for file in filenames:
    with open('us_names/' + file) as fh:
        for line in fh:

            # separate data by user specified sex
            if sex in line:
                n, s, x = line.strip().split(',')
                # using dictionary, tally records for specified sex
                if n not in records:
                    records[n] = [1, x]
                else:
                    records[n] += [1, x]

pp(records)
quit()

# open file and search for user inputted name
for file in filenames:
    with open('us_names/' + file) as fh:
        for line in fh:
            n, s, x = line.split(',')
            if sex == s:
                if n not in records:
                    records[n] = 1
                else:
                    records[n] += 1

# records = {key: val for key, val in records.items() if val > 1}
# print(f"Num of records found: {len(records)}")

max_recs = {}
for k, v in records.items():
    if v > 1:
        max_recs[k] = v

# call the max function
max_val, names = maximum_keys(max_recs)

print(f"Num of records found: {len(max_recs)}")
print(f"Max value is {max_val}")
print(f"Num of names: {len(names)}")
# pp(names)

quit()

# --------------- D U P L I C A T E S -------------------
'''
    This section of script finds the number of duplicate
    names in a specific filename.
'''

seen = {}
dupes = []

with open('us_names/' + filename) as fh:
    for line in fh:
        n, s, x = line.split(',')
        if n not in seen:
            seen[n] = 1
        else:
            if seen[n] == 1:
                dupes.append(n)
            seen[n] += 1

# pp(seen)

for name, num in seen.items():
    if num >= 3:
        print(f"{name} | {num}")

print(f"Number of duplicates: {len(dupes)} of {len(seen)}")
