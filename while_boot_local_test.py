import time
from tqdm import tqdm


def get_mode(prompt=None):
    prompts = ['>', '#', 'login']

    if prompt in prompts:
        return prompt
    else:
        return None


start = time.time()
router = open("boot.file.o", "r")
booting = True

# stages
secs_list = [2, 3, 5]
cycles = 3
stage_delays = [y for y in secs_list for x in range(cycles)]
idx = len(stage_delays)
print(stage_delays)

# status bar
bar = 100
intv_bar = 3
full_bar = bar
print()
pbar = tqdm(total=bar)
delay = 2
data_lst = []

# booting loop
while booting:
    for line in router:
        mode = get_mode(line.strip())

        if mode is None:
            pbar.update(intv_bar)
            full_bar -= intv_bar
            if idx > 0:
                idx -= 1
                secs = stage_delays[idx]
            else:
                secs = 2
            time.sleep(secs)
            data_lst.append(secs + 3)
        else:
            booting = False
            pbar.update(full_bar)
            break
    else:
        booting = False
        print('Prompt not found')
pbar.close()
print(data_lst)
print("\nDone @", time.time() - start)
print("Sum of elements in list is :", sum(data_lst))
quit()


for line in router:
    if idx > 0:
        idx -= 1
        print('sleeping for {} / {}'.format(stage_delays[idx], idx))
    else:
        print('sleeping for {} / {}'.format(stage_delays[0], idx))
quit()

while booting:
    for line in router:
        mode = get_mode(line.strip())

        if mode is None:
            pbar.update(intv_bar)
            full_bar -= intv_bar
            if stage_cycle != 0:
                d = stage_delays[stage_cycle]
                # print('sleeping for {}'.format(d))
                if i != 1:
                    # print("rnd:", i)
                    i += 1
                else:
                    stage_cycle -= 1
                    i = 0
            else:
                d = stage_delays[0]
                # print('default sleep {}'.format(d))
            time.sleep(d)
        else:
            booting = False
            pbar.update(full_bar)
            break
    else:
        booting = False

pbar.close()
print("\nDone @", time.time() - start)
# print("line:", line)
quit()
