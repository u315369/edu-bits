#!/usr/bin/python3
#
# load device configuration
#
import sys
import json
from napalm import get_network_driver


def die(*msg_list):
    """ abort program with error message """
    error_msg = ' '.join(str(x) for x in msg_list)
    sys.exit(error_msg.rstrip("\n\r"))


# get command line parameter
if len(sys.argv) != 2:
    die("Usage: get_conf hostname")
hostname = sys.argv[1]

# load device parameter database
try:
    with open("devices", "r") as f:
        dev_db = json.load(f)
except (ValueError, IOError, OSError) as err:
    die("Could not read the 'devices' file:", err)

# get device parameter
try:
    dev_param = dev_db[hostname.lower()]
except KeyError:
    die("Unknown device '{}'".format(hostname))

# load device driver
driver = get_network_driver(dev_param['type'])

# load config
with driver(dev_param['IP'], dev_param['user'], dev_param['password']) as device:
    conf = device.get_config()
    print(conf['running'])
