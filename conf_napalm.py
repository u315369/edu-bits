import sys
import napalm

if len(sys.argv) < 2:
    print('Please supply the full path to "new_good.conf"')
    sys.exit(1)
config_file = sys.argv[1]

# Use the appropriate network driver to connect to the device:
driver = napalm.get_network_driver("ios")

# Connect:
device = driver(
    hostname="192.168.1.100",
    username="admin",
    password="Cisco",
)

print("Opening ...")
device.open()


conf = device.get_config()
print(conf)
print()
print(conf['running'])

print("Loading replacement candidate ...")
device.load_replace_candidate(filename=config_file)

# close the session with the device.
device.close()
print("Done.")
