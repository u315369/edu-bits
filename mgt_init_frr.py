import telnetlib
import time
from myNetMods import get_mode, get_in
import logging


def get_in_confmode(connobj, hostname='',
                    cfg_prompt='(config)#', cmd='config t'):
    """Enter into configuration mode.

    :param connobj: Telnet object to use once established with device
    :type connobj: telnetlib connection object

    :return: True or False and configuration prompt
    :rtype: Tuple
    """
    cmd = cmd + '\n'
    conf_mode = True
    logging.info("[cf] Entering config mode")
    if hostname != '':
        cfg_prompt = '{}{}'.format(hostname, cfg_prompt)  # update prompt
        msg = 'hostname set to {}'.format(hostname)
    else:
        msg = 'hostname not set'
    logging.info("[cf] {}".format(msg))

    _r = get_mode(connobj)
    if _r != 'config':
        connobj.write(cmd.encode())
        _r = connobj.read_until(cfg_prompt.encode(), timeout=2)
        logging.debug("[cf] recvd output: {}".format(_r))

        _rx = _r.decode().splitlines()  # clean rec'd data into list
        _rx_prompt = _rx[-1]    # prompt is last item in list
        logging.info("[cf] assumed config-mode prompt, {}".format(cfg_prompt))

        if cfg_prompt not in _r.decode():
            logging.critical("[cf] failed to enter config mode")
            logging.critical("[cf] prompt recvd: {}, expected: {}"
                             .format(_rx_prompt, cfg_prompt))
            conf_mode = False
    logging.info("[cf] config mode is {}".format(conf_mode))
    return (conf_mode, cfg_prompt)


def frr_leaf_common_cfg(gw='192.168.122.1', ospf_intf='ens3'):

    # configure default gateway
    tn.write('\n\n'.encode())
    gw_cmd = f"ip route 0.0.0.0 0.0.0.0 {gw}\n\n"
    tn.read_until(')#'.encode(), timeout=5)
    tn.write(gw_cmd.encode())
    tn.read_until(')#'.encode(), timeout=5)

    # turn on ip forwarding
    tn.write('ip forwarding\n\n'.encode())
    tn.read_until(')#'.encode(), timeout=5)

    # basic ospf
    ospf_cmd = f"interface {ospf_intf}\n\n"
    tn.write('router ospf\n\n'.encode())
    tn.read_until(')#'.encode(), timeout=5)
    tn.write(ospf_cmd.encode())
    tn.read_until(')#'.encode(), timeout=5)
    tn.write('ip ospf area 0\n\n'.encode())
    tn.read_until(')#'.encode(), timeout=5)

    # bgp configuration
    tn.write('router bgp 65000\n\n'.encode())
    tn.read_until(')#'.encode(), timeout=5)
    tn.write('neigh 1.1.1.1 remote-as 1\n\n'.encode())
    tn.read_until(')#'.encode(), timeout=5)
    tn.write('neigh 1.1.1.1 update-source lo\n\n'.encode())
    tn.read_until(')#'.encode(), timeout=5)
    tn.write('address-family l2vpn evpn\n\n'.encode())
    tn.read_until(')#'.encode(), timeout=5)
    tn.write('neigh 1.1.1.1 activate\n\n'.encode())
    tn.read_until(')#'.encode(), timeout=5)
    tn.write('advertise-all-vni\n\n'.encode())
    tn.read_until(')#'.encode(), timeout=5)
    tn.write('end\n\n'.encode())

    return True


def frr_spine_cfg():
    mgt_conf = [
        "router bgp 65000",
        "bgp router-id 1.1.1.1",
        "bgp log-neighbor-changes",
        "neighbor RRfabric peer-group",
        "neighbor RRfabric remote-as 65000",
        "neighbor RRfabric capability extended-nexthop",
        "neighbor RRfabric update-source 1.1.1.1",
        "bgp listen range 11.11.11.0/24 peer-group RRfabric",
        "!",
        "address-family l2vpn evpn",
        "neighbor RRfabric activate",
        "neighbor RRfabric route-reflector-client",
        "exit-address-family",
        "!"
    ]

    intf_conf = [
        "interface ens3",
        "ip address 20.20.12.1/30",
        "no shut",
        "interface ens4",
        "ip address 20.20.13.1/30",
        "no shut",
        "interface ens5",
        "ip address 20.20.14.1/30",
        "no shut",
        "!",
        "router ospf",
        "network 0.0.0.0/0 area 0",
    ]

    for line in intf_conf:
        statement = f"{line}\n\n"
        tn.write(statement.encode())
        tn.read_until(')#'.encode(), timeout=5)

    for line in bgp_conf:
        statement = f"{line}\n\n"
        tn.write(statement.encode())
        tn.read_until(')#'.encode(), timeout=5)

    return

# logging.basicConfig(level=logging.DEBUG)
# logging.basicConfig(filename='test.log', level=logging.DEBUG)
# logger = logging.getLogger("netmiko")


start = time.time()
host = 'localhost'
dev_type = None

# dictionary of initial ports and config
device_init_cfg = {
    'IOU-MGMT': {'port': '4000', 'mgt_ip': '192.168.1.10', 'eth_ip': '', 'lo_ip': '', 'lo_int': 'lo', 'cfg?': False},
    'Spine-00': {'port': '5000', 'mgt_ip': '192.168.122.10', 'eth_ip': '', 'lo_ip': '1.1.1.1', 'lo_int': 'lo', 'cfg?': True},
    'Leaf-01': {'port': '5005', 'mgt_ip': '192.168.122.11', 'eth_ip': '20.20.12.2', 'eth_int': 'ens3', 'lo_ip': '11.11.11.1', 'lo_int': 'lo', 'cfg?': True},
    'Leaf-02': {'port': '5011', 'mgt_ip': '192.168.122.12', 'eth_ip': '20.20.13.2', 'eth_int': 'ens3', 'lo_ip': '11.11.11.2', 'lo_int': 'lo', 'cfg?': True},
    'Leaf-03': {'port': '50061', 'mgt_ip': '192.168.122.13', 'eth_ip': '20.20.14.2', 'eth_int': 'ens3', 'lo_ip': '11.11.11.3', 'lo_int': 'lo', 'cfg?': True},
}

# pass / fail dict of devices
device_report_dict = {}

# telnet / console to device.
for key in device_init_cfg:
    port = device_init_cfg[key]['port']
    hostname = key
    mgt_ip = device_init_cfg[key]['mgt_ip']
    # eth_ip = device_init_cfg[key]['eth_ip']
    # lo_ip = device_init_cfg[key]['lo_ip']
    # lo_int = device_init_cfg[key]['lo_int']

    try:
        tn = telnetlib.Telnet(host, port, timeout=2)
        # tn.set_debuglevel(1)
        # print(tn.get_socket())
    except Exception as e:
        device_report_dict[port] = False
    else:
        device_report_dict[port] = True

    # login routine for devices
    if device_report_dict[port]:
        device_state, curr_prompt = get_mode(tn)
        if device_state:
            enbl_prompt = f"{hostname}#"
            print(f"\n{hostname}, connected")
            print(f'  - device port: {port}')
            print(f'  - device enable prompt: {enbl_prompt}')
            print(f'  - device current prompt: {curr_prompt.decode()}')

            # Arista login
            if 'user' in device_state:
                get_in(tn, "admin", "Cisco")
                prompt = '>'
            # FRRouter login
            if 'login' in device_state and 'frr' in curr_prompt.decode():
                get_in(tn, "frradmin", "frr")
                prompt = '#'
            # no login required, device in ready state
            if device_state is not None:
                prompt = curr_prompt.decode()
            # verify access was granted
            verify = get_mode(tn)[0]
            if 'priv' in verify.lower():
                print(f'  - device mode: {verify}')
            else:
                raise SystemExit(f'\nERROR: Could not login to device, verify credentials.')
        else:
            raise SystemExit(f'\nERROR: Device state could not be determined. Exiting')

        # configure oob mgt on all device
        if get_in_confmode(tn):
            tn.write('hostname '.encode() + hostname.encode() + '\n\n'.encode())
            tn.read_until(')#'.encode(), timeout=5)
            tn.write('interface ens10'.encode() + '\n\n'.encode())
            tn.read_until(')#'.encode(), timeout=5)
            tn.write('ip address '.encode() + mgt_ip.encode() + '/24\n\n'.encode())
            tn.read_until(')#'.encode(), timeout=5)
            tn.write('ip forwarding\n\n'.encode())
            tn.read_until(')#'.encode(), timeout=5)
            tn.write('end\n\n'.encode())
            tn.read_until('_bs$%'.encode(), timeout=2)

# display failed devices
print("\n{:%<70}\n".format(''))
print('  Bypassed or failed ports report: ')
for port, status in device_report_dict.items():
    if not status:
        print('\t-- Port {}, {}'.format(port, status))
    # print(f"port: {port} and status: {status}")
print('')
print(" runtime: {0:.2f}".format(time.time() - start))
