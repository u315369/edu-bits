import os
import time
import json
from pprint import pprint as pp
from netmiko import ConnectHandler
import concurrent.futures
# from multiprocessing import Process, current_process


def comp(base_d, real_d):
    """Compare and contrast values.

    :param data: The content data to be parsed
    :type data: dict object

    (300) Info: Generally useful information to log. Useful but typically usually don't care.
    (200) Warn: Anything that can cause application oddities, i.e. switching from a primary to backup server or retrys.
    (100) Error: Anything fatal to the operation, i.e. can't open a required file, missing data, etc.

    :return:
    :rtype:
    """
    ret_dict = {}

    # determine if at least 1 device in common between both dicts
    devs_in_common = set(base_d.keys()) & set(real_d.keys())
    if not devs_in_common:
        ret_dict = {'SEV200':
                    {'msg': f"No like devices for valid comparison"}
                    }
        return ret_dict

    # prepare the return dictionary
    for device in devs_in_common:
        ret_dict[device] = {}

    # compare data on a per command bases for devices 'in common'
    for device in devs_in_common:
        for cmd in base_d[device]:
            if real_d.get(device, {}).get(cmd):
                realtime_cmd_data = real_d.get(device, {}).get(cmd)
                diff = set(base_d[device][cmd]) - set(realtime_cmd_data)
                ret_dict[device][cmd] = diff if diff else 'No Diff'
            else:
                ret_dict[device][cmd] = 'Command not in baseline'

    return ret_dict


def gather_vitals(vital_cmds, nc):
    ret_output = {}

    for cmdType, cmd in vital_cmds.items():
        # print(f"  - running:  {cmd}")
        output = nc.send_command(cmd)
        ret_output[cmdType] = output.split()

    return ret_output


def ssh_conn(ip):
    # pid = os.getpid()
    dev_output = {}

    try:
        # ssh to device then disconnect
        with ConnectHandler(device_type='cisco_ios', host=ip, username='admin', password='Cisco') as nc:
            if ip.endswith(('101', '102')):
                r = gather_vitals(cmds['cisco'], nc)
                hostname = 'RT1' if ip == '192.168.122.101' else 'RT2'
            else:
                r = gather_vitals(cmds['arista'], nc)
                hostname = 'SW1'
            # populate dictionary with dict of output
            dev_output[hostname] = r
            status = '\033[0;32m OK \033[0m'  # green text color
    except Exception as e:
        val = f"SSH failed on device {ip} {e}"
        dev_output[ip] = {'ERROR': val}
        status = '\033[0;31m FAILED \033[0m'  # red text color

    print(f"\n SSH to device {ip} -{status}")

    return dev_output


if __name__ == '__main__':

    start = time.time()
    dev_ips = ('192.168.122.101', '192.168.122.104', '192.168.122.102')
    process_lst = []
    dev_output = {}
    dev_errors = {}

    cmds = {
        "cisco": {
            # "hostname": "sh run | inc hostname",
            "eth1/10": "sh ip int brief | inc 1/10",
            "loopback0": "sh ip int brief | inc back0",
            "cpu": "show process | inc CDP",
            "broadcast": "show inter fa1/10 | inc broadcast",
            "cdp_lldp": "show cdp neighbors",
        },
        "arista": {
            "cpu": "show process | inc LLDP",
            "broadcast": "show inter man1 | inc broadcast",
            "cdp_lldp": "show lldp neighbors",
        },
        "all": {
            "logging": "show logging | inc informational",
        },
    }

    # new method of running multi-processes and threading
    with concurrent.futures.ProcessPoolExecutor() as executor:
        # maps the function to each ip in list
        results = executor.map(ssh_conn, dev_ips)

    '''
    NOTE: The return varible 'results' is a generator object.
    A generator is a function that returns an object (iterator)
    which we can iterate over (one value at a time).
    '''
    for keyval in results:
        for _k, _v in keyval.items():
            if 'ERROR' in _v:
                dev_errors.update(keyval)
            else:
                dev_output.update(keyval)

    # determine if any device returned data output before proceeding
    if not dev_output:
        print(f'\n All devices failed SSH connection attempt. Exiting script.')
        exit(f'\n Runtime : {time.time() - start:+.2f} secs\n')

    # write vitals to JSON file, if it doesn't exist
    try:
        with open('baseline.json', 'r') as fh:
            baseline = json.load(fh)
    except Exception:
        print(f'\n\n MISSING FILE - Creating new baseline file')
        baseline = None
        with open('baseline.json', 'w') as fh:
            json.dump(dev_output, fh, indent=2)

    if not baseline:
        print(f' No comparison required')
        exit(f'\n Runtime : {time.time() - start:+.2f} secs\n')

    # compare and contrast function
    r_dict = comp(baseline, dev_output)

    # format comp funtion output
    print(f'* ' * 35)

    if r_dict.get('SEV200'):
        msg = r_dict.get('SEV200', {}).get('msg')
        print(f' WARNING - {msg}')
        exit(f'\n Runtime : {time.time() - start:+.2f} secs\n')

    print(f'\n Differences Breakdown\n')
    print(f'* ' * 35)

    for cmd_key, cmd_val in r_dict.items():
        print(f' Device Name: {cmd_key.upper()}')
        for cmd_type, cmd_data in cmd_val.items():
            print(f"  - {cmd_type.upper():<10} :: {cmd_data}")
        print()

    print(f'\n Runtime : {time.time() - start:+.2f} secs\n')

    # Older method of running multi-processes and threading
    # for ip in dev_ips:
    #     print(f"\n Attempting SSH to device {ip}")
    #     my_process = Process(target=ssh_conn, args=(ip,))
    #     process_lst.append(my_process)
    #     my_process.start()

    # for process in process_lst:
    #     process.join()

    # print the process list
    # print(f"\n Process list:")

    # rx8ZrBVY34fT

'''
# For a set of network devices, gather info/data
## the info/data will be :
    ## CPU Utilization
    ## Interface Statistics - tx and rx
    ## Is logging turned on?
    ## Check CDP and/or LLDP neighbors
    !! Refresh baseline file after set period of time.
    !! Compare changes between running cfg baseline vs realtime
        !!! Username changes
        !!! mask/encrypt saved baseline cfg.
    !! Up/Down status for Layer-3 protocols (RIP, OSPF, BGP)
    ## NOTE: This data can be anything pulled from device!

# !! Verify you can connect to each device

# !! Validate that you have collected the appropriate data.

# The device output should be stored on disk in JSON format, if not
# stored previously.

# Use stored data to compare against realtime gathered info/data.

# Capture the difference between the compared data

# Display the difference to screen

# Once complete, the test should be to explain the output produced
# from the comparison. The explanation should be good enough to follow
# the example of engineer to manager.
'''
