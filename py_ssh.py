from netmiko import Netmiko
import telnetlib
import time
import platform
import subprocess
import socket
from lib.netmods import checkSSH, get_ping, init_lab_dev


def showDict(d):
    """Print dictionary generated from yaml"""
    from pprint import pprint as pp
    pp(d)


def normalizeData(raw_data, key_val=None):
    """Clean up command generated output"""
    assert type(raw_data) is dict, "normalize data content is invalid type"
    scrub_list = []

    # unpack, scrub, and re-package raw_data
    outp = raw_data[key_val].encode('ascii')
    scrub_list = outp.splitlines()
    return scrub_list


def is_alive(host, opt='c'):
    """determine network connectivity"""

    try:
        subprocess.check_output(
            "ping -W 2 -{} 1 {}".format(opt, host), shell=True)
    except Exception as e:
        return False
    return True


def get_ip():
    """Obtain local IP address"""

    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(('192.168.255.254', 1))
        IP = s.getsockname()[0]
    except:
        IP = '127.0.0.1'
    finally:
        s.close()
    return IP


my_device_dict = {
    "host": "",
    "username": "admin",
    "password": "Cisco",
    "device_type": "cisco_ios",
    "ssh_config_file": "./proxy_ssh_cfg"
}

my_device_list = [
    '192.168.1.120',
    '192.168.1.100',
    '192.168.1.110',
]

start = time.time()
proxy = '192.168.0.19'
proxy_prt = '5003'

status_chk_dict = {
    'Proxy': {},
    'Ping_results': {},
    'Telnet': {}
}

# check if proxy is up
local_ip = get_ip()
if '10.8.0.' in local_ip:
    print('ALERT: Localhost connected via VPN...bypassing ping test.')
elif is_alive(proxy, 'c' if platform.system().lower() == "linux" else 'n'):
    status_chk_dict['Proxy']['alive?'] = 'True'
    print('\nProxy box is up and alive:', proxy)
else:
    raise SystemExit(
        'CRITICAL: SSHProxy host is unreachable, {}'.format(proxy))

# check if ssh service is enabled and ping connectivity
print('\nChecking other services and devices . . .')
try:
    tn = telnetlib.Telnet('192.168.0.21', proxy_prt, timeout=2)
except Exception as e:
    raise SystemExit(
        '\nERROR: Telnet failed to port {}, {}'.format(proxy_prt, e))

ret = checkSSH(tn)
status_chk_dict['Proxy']['ssh'] = ret

# test ping from proxy host
for ip in my_device_list:
    _r = get_ping(tn, ip)
    ploss = _r['PING'].split(',')[2].strip()
    status_chk_dict['Ping_results'][ip] = ploss

# validate and report results for proxy
if status_chk_dict['Proxy']['ssh']:
    print('  * Proxy SSH services: Active')

# validate results on gns routers/switches
for _ip, _res in status_chk_dict['Ping_results'].items():
    if '100%' in _res:
        print('  * Device {}, IP unreachable: requires init configuration'.format(_ip))
        run = init_lab_dev(_ip)
        status_chk_dict['Telnet'][_ip] = run
    else:
        print('  * Device {}, configuration: GOOD'.format(_ip))
        status_chk_dict['Telnet'][_ip] = True

# connect to device(s)
for ip in my_device_list:
    my_device_dict['host'] = ip

    if status_chk_dict['Telnet'][ip]:
        print('\nConnecting to', ip)
        try:
            net_connect = Netmiko(**my_device_dict)
        except Exception as e:
            print('Device ' + ip + ' connection error')
        else:
            # list of commands
            cmd_list = ['show ip int brief', 'who', 'show clock', 'show ip ro']
            i = 1

            for cmd in cmd_list:
                print('\n  command {} --> {}\n'.format(i, cmd))
                output = net_connect.send_command(cmd)
                print(output)
                print("{:%<70}\n".format(''))
                i += 1

                if cmd == 'who':
                    # clear vty lines (disconnect) from device
                    line_output = {}
                    host = my_device_dict['host']
                    line_output[my_device_dict['host']] = output
                    net_connect.enable()  # Ensure in enable mode
                    vty_data = normalizeData(line_output, host)

                    for line in vty_data:
                        if 'vty' in line.decode('ascii'):
                            if '*' not in line.decode('ascii'):
                                cmd = 'clear line vty {}'.format(
                                    line.decode('ascii').split()[2])
                                print('clearing: ', cmd)
                                d = net_connect.send_command(
                                    cmd, expect_string='onfirm')
                                print(d)
                                net_connect.send_command('\n')

print("\nTime elapsed: {:.2f}\n".format(time.time() - start))
# nc.disconnect()
# print("\nTime elapsed: {:.2f}\n".format(time.time() - start))
