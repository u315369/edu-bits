from gns3fy import Gns3Connector, Project, Node, Link
from pprint import pprint as pp
from tabulate import tabulate

GNS3_URL = "http://localhost:3080"
PROJ_NAM = "base-4devices"

# define the connector object
server = Gns3Connector(url=GNS3_URL)

# verify connectivity by checking server ver
print(f"{server.get_version()}\n")

# obtain list of projects from gns3 server
print()
print(tabulate(server.projects_summary(is_print=False),
               headers=["Projx", "ID", "Nodes", "Lnks", "Status"]))

# grab a project from the server and display
lab = Project(name=PROJ_NAM, connector=server)
lab.get()

# show some of its attributes
print(f"\nName: {lab.name}\n -- Status: {lab.status}\n \
      -- Is auto_closed?: {lab.auto_close}")

# retrieve node info in dictionary format
print(f"\nINVENTORY OF NODES :")
pp(lab.nodes_inventory())

# node info within the lab
print(f"\nNODES PROJECT SUMMARY :")
print(lab.nodes_summary())
print(f"\nSUMMARY:")
for each in lab.nodes_summary(is_print=False):
    dev_name, dev_status, dev_port, dev_id = each
    print(f"Name: {dev_name}\n -- Port No.: {dev_port}\n -- ID: {dev_id}\n")

# parse info from summary
for each in lab.nodes_summary(is_print=False):
    dev_name, dev_status, dev_port, dev_id = each
    if dev_name.lower() == "frr-02":
        print(f"Parse and Start Device:")
        print(f"Device name: {dev_name}")
        print(f" -- ID: {dev_id}\n")
        # display the properties for the device
        pp(lab.get_node(name=dev_name).properties)
