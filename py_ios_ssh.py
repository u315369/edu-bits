import telnetlib

'''
    hostname <host>
    ip domain name <domain>
    crypto key generate rsa
    How many bits in the modulus [512]: 1024/2048
    ip ssh version 2
    !
    aaa new-model
    aaa authentication login default local
    aaa authorization exec default local
    !
    username <config_user> privilege 15 secret <password>
    !
    line vty 0 4
    transport input ssh
    !
    ! for testing only: no authentication on console
    aaa authentication login no_auth none
    line con 0
    privilege level 15
    login authentication no_auth
'''

prompt = '{}(config)#'.format(hostname)  # update prompt

# if 'fig)#' not in prompt.decode():
logging.info("Entering config mode")
connObj.write('conf t\n'.encode())
connObj.read_until(prompt.encode(), timeout=5)

# setup mgmt interface
connObj.write('int eth0/0'.encode() + '\n\n\n'.encode())
connObj.write('ip add 192.168.1.120'.encode() +
              ' 255.255.255.0\n'.encode())
connObj.write('no shut'.encode() + '\n\n'.encode())
connObj.write('exit'.encode() + '\n\n'.encode())
connObj.read_until(prompt.encode(), timeout=5)

# setup ssh configuration
feedback = [b"modulus [512]:", b"replace them"]
connObj.write('hostname IOU-1'.encode() + '\n\n\n'.encode())
connObj.write('ip domain name lab.io'.encode() + '\n\n'.encode())
connObj.write('crypto key generate rsa'.encode() + '\n'.encode())
_idx, _obj, _match = connObj.expect(feedback, timeout=5)
if _idx == 1:
    logging.info("RSA keys already defined. Replacing")
    connObj.write('yes'.encode() + '\n'.encode())
    logging.info("Entering module size of 1024")
    connObj.write('1024'.encode() + '\n'.encode())
    connObj.write('ip ssh version 2'.encode() + '\n\n'.encode())
    connObj.read_until(prompt.encode(), timeout=5)
    connObj.write('aaa new-model'.encode() + '\n'.encode())
    logging.info("Applying AAA configuration")
    connObj.read_until(prompt.encode(), timeout=5)
    connObj.write(
        'aaa authentication login default local'.encode() + '\n\n'.encode())
    connObj.write(
        'aaa authorization exec default local'.encode() + '\n\n'.encode())
    connObj.write(
        'username admin privilege 15 secret Cisco'.encode() + '\n\n'.encode())
    connObj.write('line vty 0 4'.encode() + '\n\n'.encode())
    connObj.write('transport input ssh'.encode() + '\n\n'.encode())
    connObj.read_until(prompt.encode(), timeout=5)
    connObj.write('end'.encode() + '\n'.encode())
    return
