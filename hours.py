import sql_hrs
import random
from datetime import datetime
from enum import Enum


# Using enum class to create enumerations
class Weekdays(Enum):
    SUN = 0
    MON = 1
    TUE = 2
    WED = 3
    THU = 4
    FRI = 5
    SAT = 6


class Constants(Enum):
    WEEK = 7 * 86400
    DAY = 86400


MENU_PROMPT = """-- Time Sheet App --

Please select from option below:

1) Add hours.
2) See all hours.
3) Find Duplicates.
4) Exit.

Your selection: """


def menu():
    connection = sql_hrs.connect()
    sql_hrs.create_tables(connection)

    # Python walrus operator (https://realpython.com/lessons/assignment-expressions/)
    while (user_input := input(MENU_PROMPT)) != '4':
        if user_input == '1':
            ret = create_week_hrs()
            wk_beg = ret[0][0]
            wk_end = int(wk_beg + (86400 * 6))
            sun, sat = ret[0][1], ret[6][1]
            mon, tue = ret[1][1], ret[2][1]
            wed, thr = ret[3][1], ret[4][1]
            fri = ret[5][1]

            sql_hrs.add_hours(connection, wk_beg, wk_end,
                              sun, mon, tue, wed, thr, fri, sat)

        elif user_input == '2':
            hrs = sql_hrs.get_all_hrs(connection)
            print(f'\nRecords:')
            for row in hrs:
                date_s = datetime.fromtimestamp(row[0]).strftime("%m-%d")
                date_e = datetime.fromtimestamp(row[1]).strftime("%m-%d")
                yr = datetime.fromtimestamp(row[1]).strftime("%Y")
                print(f' Week of {date_s} thru {date_e} ({yr})', end="")
                print(f' -- ', end="")
                tot = 0
                for x in range(2, 9):
                    print(f'{row[x]}', end="  ")
                    tot = tot + row[x]
                print(f'[{tot}]')
            print(f'\nTotal Records: {len(hrs)}\n')

        elif user_input == '3':
            dupes = sql_hrs.get_dupes(connection)
            for row in dupes:
                date_s = datetime.fromtimestamp(row[0]).strftime("%m-%d-%Y")
                print(f'{row} | {date_s}')

        else:
            print('\nInvalid input, please try again!\n')


def make_work_hrs():
    hrs = []

    for day in Weekdays:
        hr = random.randint(7, 10) + .5 if 1 <= day.value < 6 else 0
        hrs.append(hr)
    return(hrs)


def make_wkbegin_ts():
    # create random but VALID date
    try:
        y, m, d = 2021, random.randint(1, 12), random.randint(1, 31)
        datetime(y, m, d)
    except ValueError:
        exit(f"Invalid date {y}-{m}-{d}")

    # get the week-begin SUN date from random date
    dow = datetime(y, m, d).strftime('%a')
    _ts = datetime(y, m, d).timestamp()
    for day in Weekdays:
        if dow.upper() == day.name:
            return (int(_ts - (Constants.DAY.value * day.value)))


def create_week_hrs():
    wk_days = []
    for _ in range(1):
        wb_ts = make_wkbegin_ts()
        wrk_hrs = make_work_hrs()

        # get dates Sun thru Sat
        for n in range(7):
            wk_days.append((Constants.DAY.value * n) + wb_ts)

    r = list(zip(wk_days, wrk_hrs))
    return r


menu()
quit()

# print enum keys and values
print(list(Weekdays))

for day in Weekdays:
    print(f'{day.name:<5s} => {day.value}')


# TESTING - print all enum members using loop
print("The enum members are : ")
for weekday in Weekdays:
    print(weekday)

hrs = []

for day in Weekdays:
    hr = random.randint(7, 10) + .5 if day.value < 6 else 0
    print(f' ({day.value}) {day.name} -> {hr}')
    hrs.append(hr)

print(f'-' * 20)
print(f' Total HRs: {sum(hrs)}\n')

print(datetime.today())

# determine the day of the week from date
date_array = [
    (2021, 4, 22),
    (2020, 5, 22),
    (2020, 7, 24),
    (2020, 7, 25),
    (2020, 7, 26),
]

wb_dates = []

for tup in date_array:
    y, m, d = tup
    dow = datetime(y, m, d).strftime('%a')
    ts = datetime(y, m, d).timestamp()
    for day in Weekdays:
        if dow.upper() == day.name:
            wb_ = datetime.fromtimestamp(ts - (86400 * day.value))
            wb_dates.append(wb_)
            print(f"Found * {dow} | Day num * {day.value}", end='')
            print(f" | WB Date * {wb_}")

# determine number of weeks between dates
current_date_ts = datetime.today().timestamp()
for d in wb_dates:
    print(f"Diff weeks: {((current_date_ts - d.timestamp()) / 86400) // 7}")

print(datetime.fromtimestamp(current_date_ts))
for _ in range(4):
    current_date_ts = current_date_ts - (Constants.WEEK.value)
    print(datetime.fromtimestamp(current_date_ts))

print(Constants.DAY.value)
