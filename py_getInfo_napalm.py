import json
from pprint import pprint as pp
from napalm import get_network_driver


driver = get_network_driver('ios')

my_device1 = driver('192.168.1.120', 'admin', 'Cisco')
my_device1.open()

print('**** Get Facts ****')
ios_output = my_device1.get_facts()
print(json.dumps(ios_output, indent=4))

print('\n**** Get Interfaces ****')
ios_output = my_device1.get_interfaces()
print(json.dumps(ios_output, sort_keys=True, indent=4))

print('\n**** Get Counters ****')
ios_output = my_device1.get_interfaces_counters()
print(json.dumps(ios_output, sort_keys=True, indent=4))

print('\n**** Get ARP ****')
ios_output = my_device1.get_arp_table()
print(json.dumps(ios_output, indent=4))
