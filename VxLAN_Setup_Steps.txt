
===========================================
-
- The following info sets up a static VxLAN
- tunnel between 2-linux hosts/routers.
- The setup requires: FRR for the control plane
- routing and brctl for the layer-2 interface
- (sudo apt install bridge-utils)
-
===========================================

*** Config Host-1 : lo addr. 2.2.2.2 ***

Step 1: Create the vxlan interface with VNI (id xx)
   ip link add vxlan6010 type vxlan id 10 dstport 4789

Step 2: Create 'static' connection to tunnel peer 3.3.3.3
   bridge fdb append to 00:00:00:00:00:00 dst 3.3.3.3 dev vxlan6010

Step 3: Assign address to vxlan intf & turn it up
   ip addr add 23.23.23.2/24 dev vxlan6010
   ip link set up dev vxlan6010

Step 4: Create bridge intf on linux router
   sudo brctl addbr br6010

Step 5: Add an intf to newly created bridge
   sudo brctl addif br6010 vxlan6010
   sudo brctl addif br6010 ens9

Step 6: Tune bridge intf ie STP and MAC aging (in seconds)
   sudo brctl setageing br6010 45
   sudo brctl stp br6010 off

Step 7: Turn up the bridge intf (may be optional?!)
   sudo ip link set up dev br6010


*** Config Host-2 : lo addr 3.3.3.3 ***

Step 1: Create the vxlan interface
   sudo ip link add vxlan6010 type vxlan id 10 dstport 4789

Step 2: Create 'static' entries/conns to tunnel peers
   sudo bridge fdb append to 00:00:00:00:00:00 dst 2.2.2.2 dev vxlan6010

Step 3: Assign address to vxlan intf & turn it up
   sudo ip addr add 23.23.23.3/24 dev vxlan6010
   sudo ip link set up dev vxlan6010

Step 4: Create bridge intf on linux router
   sudo brctl addbr br6010

Step 5: Add an intf to newly created bridge
   sudo brctl addif br6010 vxlan6010
   sudo brctl addif br6010 ens9

Step 6: Tune bridge intf ie STP and MAC aging (in seconds)
   sudo brctl setageing br6010 45
   sudo brctl stp br6010 off

Step 7: Turn up the bridge intf (may be optional?!)
   sudo ip link set up dev br6010

