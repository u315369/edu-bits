import random
import bisect
from datetime import datetime
from enum import Enum
from pprint import pprint as pp


# Using enum class to create enumerations
class Weekdays(Enum):
    SUN = 0
    MON = 1
    TUE = 2
    WED = 3
    THU = 4
    FRI = 5
    SAT = 6


class Constants(Enum):
    WEEK = 7 * 86400
    DAY = 86400


def make_work_hrs():
    hrs = []

    for day in Weekdays:
        hr = random.randint(7, 10) + .5 if 1 <= day.value < 6 else 0
        hrs.append(hr)
    return(hrs)


def clock_in_out():
    start_hh = random.randint(6, 9) 
    start_mm = random.randint(1, 59)
    end_hh = random.randint(13, 19)
    end_mm = random.randint(1, 59)
    start = (start_hh * 100) + (start_mm * 1)
    end = (end_hh * 100) + (end_mm * 1)
    return(start, end)


def timesheet_rules(hh, mm):
    rnd_mm = [0, 15, 30, 45, 1]
    breakpoints = [0, 7, 22, 37, 52]

    idx = bisect(breakpoints, mm)
    return rnd_mm[idx]


def make_wkbegin_ts(yr=2021):
    # create random but VALID date
    try:
        y, m, d = yr, random.randint(1, 12), random.randint(1, 31)
        datetime(y, m, d)
    except ValueError:
        exit(f"Invalid date {y}-{m}-{d}")

    # get the week-begin SUN date from random date
    dow = datetime(y, m, d).strftime('%a')
    _ts = datetime(y, m, d).timestamp()
    for day in Weekdays:
        if dow.upper() == day.name:
            return (int(_ts - (Constants.DAY.value * day.value)))


def create_week_hrs():
    wk_days = []
    for _ in range(1):
        wb_ts = make_wkbegin_ts()
        wrk_hrs = make_work_hrs()

        # get dates Sun thru Sat
        for n in range(7):
            wk_days.append((Constants.DAY.value * n) + wb_ts)

    r = list(zip(wk_days, wrk_hrs))
    return r


week_hrs = create_week_hrs()

# extract week-hours
for x in week_hrs:
    print(x[0], datetime.fromtimestamp(x[0]), x[1])

print()

# calculate total worked hours
wrk_hrs = clock_in_out()
tot = datetime.strptime(str(wrk_hrs[1]), '%H%M') - datetime.strptime(str(wrk_hrs[0]), '%H%M')
print(wrk_hrs)
print(f'Total Hrs => {tot} ')

# parse work hours start & end times
for t in wrk_hrs:
    hh = int(str(t)[:-2])
    mm = int(str(t)[-2:])
    print(f'Parsed HH:MM => {hh}T{mm}')
    

# check timesheet rules

quit()

data_dict = {}

for _ in range(2):
    hrs = create_week_hrs()
    # extract work week and hours
    for ea in hrs:
        _date = ea[0]
        _hour = ea[1]
        data_dict[_date] = {}
        data_dict[_date]['hrs'] = _hour

pp(data_dict)

