from netmiko import ConnectHandler
import json

Ethernet01 = {
    'description': "",
    'is_enabled': ["true"],
    'is_up': [],
    'last_flapped': ["-1.0"],
    'mac_address': [],
    "speed": ["10"],
}

# netmiko parameters
my_device = {
    'device_type': 'cisco_ios',
    'host': '192.168.1.100',
    'username': 'admin',
    'password': 'Cisco',
}

# setup default options
intf_dict = {}
is_enable = is_up = True
speed = '10Mbit'

# SSH connection to device
nc = ConnectHandler(**my_device)
cmd = "show interface  | include thernet"
output = nc.send_command(cmd)
interfaces = output.splitlines()

# build out dictionary with interface names
for line in interfaces:
    line = line.split()
    eth = line[0]
    if len(eth) > 8:
        intf_dict[eth] = {}

# parse mac-address per interface name
for macaddr in interfaces:
    line = macaddr.split()
    if len(line[0]) > 8:
        eth = line[0]   # assign interface name
    if '(bia' in line:
        mac = line[5]
        intf_dict[eth]['mac_address'] = mac
        intf_dict[eth]['is_enable'] = is_enable
        intf_dict[eth]['speed'] = speed

print(json.dumps(intf_dict, indent=4))
quit()

for macaddr in interfaces:
    line = macaddr.split()
    # print('===> ', line)
    if '(bia' in line:
        # print('Found!!!: ', line)
        mac = line.pop(5)
        print(mac)
        for key in intf_dict:
            print(key)
            intf_dict[key]['mac_address'] = mac

    # intf_dict[int]['mac-address'] = 'BLANCK'
    # cmd1 = "show interface  | include thernet"
    # output = nc.send_command(cmd1)
    # intf_dict[each][mac-address] =  each.split()
    # print (int)
    # intf_dict[each][mac-address] =  each.pop(-6)
    # int = output.split()
    # print (int)

# print(intf_dict)
print(json.dumps(intf_dict, sort_keys=True, indent=4))
