'''
    Me functions of useful Python snippets to code like a pro in Python.
    This snippet will also help you with daily life programming problems.
'''


def dupes(array):
    """
    Fastest way to check for duplicate in a list.

    Parameters
    ----------
    array : list
        a list of items to check against.

    Returns
    -------
    Boolean
        True or False if list has duplicates
    """

    return len(array) != len(set(array))


def merge_dict(dic1, dic2):
    """
    This snippet will merge two dictionaries into one.

    Arguments:
    ----------
    dic1 : DICT
        first dictionary of elements to merge
    dic2 : DICT
        Second dictionary of elements to merge

    Returns:
    --------
    DICT
        combine dictionary of dic1 and 2
    """

    dic3 = dic1.copy()
    dic3.update(dic2)
    return dic3


def filtering(array):
    """
    This snippet is used to remove any false related values in a list
    like false,0,None

    Arguments:
    -----------
    array : list
        a list of items to check against.

    Returns:
    ---------
    list
        List of filtered elements without 'false' values
    """

    return list(filter(None, array))


def mem_of_var(var):
    """
    This snippet is used to get the memory used by any Python
    variable

    Arguments:
    ----------
    var : str
        varible to check for memory size/usage.

    Returns:
    ---------
    int
        an integer representing the size in bytes
    """

    import sys
    return (sys.getsizeof(var))


my_list = [1, 2, 0, 4, 5, 1, False, 3, 4, 57, True, 0, "", 5, 4, 3, None, 1]
my_dict1 = {1: "hello", 2: "world"}
my_dict2 = {3: "Python", 4: "Programming"}
my_str1 = 'This is my string'
my_str2 = 'Atyus'


# Run and print the functions
print(dupes(my_list))                   # Dupes Function
print(merge_dict(my_dict1, my_dict2))   # Merge Function
print(filtering(my_list))               # Fileter Function
print(mem_of_var(my_list))              # Varible Mem Size Function
