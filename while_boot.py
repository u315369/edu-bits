import logging
import telnetlib
import time
import argparse
from tqdm import tqdm

log_fn = __file__.replace('.py', '.log')
logging.basicConfig(level=logging.DEBUG, filename=log_fn,
                    format='%(asctime)s %(levelname)-5s: %(message)s',
                    datefmt='%b %d %H:%M:%S')
logging.debug(f"##" * 27)


def cliArgs():
    """function to define and parse command line arguments """

    parser = argparse.ArgumentParser(description="Check and validate device boot status",
                                     formatter_class=argparse.RawDescriptionHelpFormatter, usage="%(prog)s [options] ... host [port]")
    parser.add_argument('host', nargs='?', metavar='host', default='localhost',
                        help='Specifies remote host to contact. Default is localhost.')
    parser.add_argument('port', help='Specifies a port number to contact.',
                        metavar='port')
    parser.add_argument('-v', '--version', action='version',
                        version='%(prog)s 1.4a')
    args = parser.parse_args()
    return args


def get_mode(connObj, prompt=None):
    """Determine network device access and priv mode.

    :param connObj: Telnet object to use once established with device
    :type connObj: telnetlib connection object
    :param prompt: The desired prompt
    :type prompt: string, optional

    :return: Tuple of priveledge mode and the prompt
    :rtype: tuple
    """
    prompts = [b'in\:', b'\)#', b'\>', b'\#', b'skip\)']

    # determine which 'mode' by prompts
    for i in range(1):
        connObj.write('\r'.encode())
        idx, obj, match = connObj.expect(prompts, timeout=3)
        _r = match.splitlines()  # clean rec'd data into list

    match = (_r[-1] if _r else 'n/a')   # retreive last item in list

    if idx == 0:
        mode = 'login'   # login prompt
    elif idx == 1:
        mode = 'config'  # conf mode
    elif idx == 2:
        mode = 'priv_1'  # privilege 1
    elif idx == 3:
        mode = 'priv_15'  # privilege 15
    elif idx == 4:
        mode = 'ESC'    # ESC key. EOS initialization (press ESC to skip)
        connObj.write('\x1b'.encode())
    else:
        mode = None
    logging.debug(f"[gm] return match, {match.decode().strip()}")
    logging.debug(f"[gm] return mode, {mode}")
    return mode, match.decode().strip()  # return mode and prompt


# check cli args
args = cliArgs()
logging.debug(f"[mn] cli args, {args}")

# init varibles
start = time.time()
port = '5005'
booting = True
host = args.host
port = args.port

# build stages for delay
secs_list = [2, 3, 5]
rep = 3
stage_delays = [y for y in secs_list for x in range(rep)]
idx = len(stage_delays)

# console to device port
print('\nConsoling to device on port ' + port + '\n')
try:
    tn = telnetlib.Telnet(host, port, timeout=2)
except Exception as e:
    raise SystemExit(
        '\nERROR: Telnet to {} on port {} failed, {}\n'.format(host, port, e))

# status bar
print()
full_bar = bar = 100
intv_bar = 3
pbar = tqdm(total=bar, desc='Boot Status')
metadata_lst = []   # connection list of function hits and secs

# booting loop
while booting:
    mode, prompt = get_mode(tn)

    if mode is None:
        pbar.update(intv_bar)
        full_bar -= intv_bar
        if idx > 0:
            idx -= 1
            secs = stage_delays[idx]
        else:
            secs = 2
        time.sleep(secs)
        logging.debug(f"[mn] pausing, {secs}s")
        metadata_lst.append(secs + 3)
    else:
        booting = False
        pbar.update(full_bar)

logging.debug(f"[mn] {metadata_lst}")
pbar.close()
print(f"\n Runtime: {'':>7}{time.time() - start:.2f}")
print(f" Function hits: {'':>4}{len(metadata_lst)}")
print(f" Prompt: {prompt}")

# animation = "|/-\\"
# idx = 0
# while True:
#     print(animation[idx % len(animation)], end="\r")
#     idx += 1
#     time.sleep(0.1)
# quit()
