import sqlite3

# create hours table
CREATE_HRS_TABLE = """
CREATE TABLE IF NOT EXISTS hours
(wkbegin_ts INTEGER, wkend_ts INTEGER, sun_hrs REAL,
    mon_hrs REAL, tue_hrs REAL,
    wed_hrs REAL, thu_hrs REAL,
    fri_hrs REAL, sat_hrs REAL);"""

# insert data into table
INSERT_HRS = """
INSERT INTO hours
(wkbegin_ts, wkend_ts, sun_hrs,
    mon_hrs, tue_hrs, wed_hrs,
    thu_hrs, fri_hrs, sat_hrs)
VALUES (?,?,?,?,?,?,?,?,?);"""

# queries
GET_ALL_HRS = "SELECT * FROM hours;"

GET_DUPLICATES = """
SELECT wkbegin_ts, COUNT(*)
FROM hours
GROUP BY wkbegin_ts
HAVING COUNT(*) > 1;"""


def connect():
    # connect to database
    return sqlite3.connect("HRsdata.db")


def create_tables(connection):
    with connection:
        connection.execute(CREATE_HRS_TABLE)


def add_hours(connection, wkbegin_ts, wkend_ts, sun_hrs, mon_hrs, tue_hrs, wed_hrs, thu_hrs, fri_hrs, sat_hrs):
    with connection:
        connection.execute(INSERT_HRS, (wkbegin_ts, wkend_ts, sun_hrs,
                                        mon_hrs, tue_hrs, wed_hrs, thu_hrs, fri_hrs, sat_hrs))


def get_all_hrs(connection):
    with connection:
        return connection.execute(GET_ALL_HRS).fetchall()


def get_dupes(connection):
    with connection:
        return connection.execute(GET_DUPLICATES).fetchall()


'''
# create a cursor object
c = conn.cursor()

# create table
sql = "CREATE TABLE IF NOT EXISTS contacts (name text, phone text, city text))"
c.execute(sql)

# insert values
sql = "INSERT INTO contacts VALUES ('Tommy', '720-877-1879', 'Denver')"
c.execute(sql)

# update values
sql = "UPDATE contacts SET city='Dallas' WHERE name='Tommy'"
c.execute(sql)

# Retrieve values
sql = "SELECT * FROM contacts"
c.execute(sql)

results = c.fetchall()
print(results)

# commit changes
conn.commit()

# close connection
conn.close()
'''
