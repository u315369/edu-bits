from netmiko import ConnectHandler
import logging

# logging.basicConfig(filename='test.log', level=logging.DEBUG)
# logger = logging.getLogger("netmiko")

my_device = {
    'host': '192.168.1.140',
    'username': 'admin',
    'password': 'Cisco',
    'device_type': 'arista_eos',
}

# SSH connection to device
nc = ConnectHandler(**my_device)

prompt = nc.find_prompt()
# Print the prompt/hostname of the device
print("Prompt: {}".format(prompt))

# Ensure we are in enable mode and can make changes.
if "#" not in prompt[-1]:
    print("Entering enable mode")
    nc.enable()

try:
    op = nc.send_config_from_file('commands_file')
    print(op)
    nc.disconnect()
except Exception as e:
    raise SystemExit(e)
