"""
This provides a easy method to obtain a network devices 'mode'
by obtaining and evaluating the devices prompt.

Intended for use with the following connection types:
    * TelnetLib with console connections
    * Console Server
"""

import re
import logging
import telnetlib
import time
from pprint import pprint as pp

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(levelname)-5s: %(message)s',
                    datefmt='%b %d %H:%M:%S')


def showDict(d):
    """Print dictionary generated from yaml"""
    pp(d)


def checkSSH(connObj):
    """
    Determine if ssh is running on linux host. If not:
     * start the service
     * create ssh directory
     * copy public key

        :param connObj - Telnet object to use once established with device
        :type connObj - telnetlib connection object

        :return - True
    """

    # determine host prompt
    connObj.write('\n\n'.encode())
    _prompt = connObj.read_until('#'.encode(), timeout=1)
    logging.debug("system prompt \"{}\"".format(
        _prompt.decode().strip()))

    cmds_dict = {
        'pub-file': {'cmd': '[[ -f /root/my_data/pub.txt ]] && echo "TRUE"\n'},
        'ssh-dir': {'cmd': '[[ -d /root/.ssh ]] && echo "TRUE"\n'},
        'auth-file': {'cmd': '[[ -f /root/.ssh/authorized_keys ]] && echo "TRUE"\n'},
        'sshd_enabled': {'cmd': 'pidof sshd ; [[ $? == 0 ]] && echo "TRUE"\n'},
        'PermitRoot': {'cmd': 'cat /etc/ssh/sshd_config | grep PermitRootLogin && echo "TRUE"\n'}, }
    cmds_pattern = re.compile(br'(^TRUE)', re.MULTILINE)
    cmd_reboot = 'service ssh start\n'
    start_ssh = False

    for key in cmds_dict:
        connObj.write(cmds_dict[key]['cmd'].encode())
        _idx, _obj, _match = connObj.expect(
            [cmds_pattern], timeout=1)
        cmds_dict[key]['remediate'] = _idx

    # public key file - validate and remediate
    if cmds_dict['pub-file']['remediate'] < 0:
        raise SystemExit('FATAL: Public key missing from proxy host')

    # root login - validate and remediate
    if cmds_dict['PermitRoot']['remediate'] < 0:
        logging.info("updating ssh conf for root access")
        connObj.write(
            'echo PermitRootLogin without-password >> /etc/ssh/sshd_config\n'.encode())
        connObj.read_until(_prompt, timeout=1)
        msg = '  - sshd conf, UPDATED'
        start_ssh = True
        print(msg)

    # ssh directory and auth file - validate and remediate
    if cmds_dict['ssh-dir']['remediate'] < 0:
        connObj.write(
            'mkdir /root/.ssh && cp /root/my_data/pub.txt /root/.ssh/authorized_keys\n'.encode())
        connObj.read_until(_prompt, timeout=5)
        msg = '  - ssh dir and auth_key, UPDATED'
    elif cmds_dict['auth-file']['remediate'] < 0:
        connObj.write(
            'cp /root/my_data/pub.txt /root/.ssh/authorized_keys\n'.encode())
        connObj.read_until(_prompt, timeout=5)
        msg = '  - ssh auth_key, UPDATED'
    else:
        msg = '  - ssh conf, GOOD'
    print(msg)

    if start_ssh or cmds_dict['sshd_enabled']['remediate'] < 0:
        connObj.write(cmd_reboot.encode())
        msg = '  - enabling ssh services'
        logging.info("enabling ssh services")
    print(msg)

    return True


def get_mode(connObj, prompt=None):
    """ Determine network device mode.

        :param connObj - Telnet object to use once established with device
        :type connObj - telnetlib connection object

        :param prompt - (optional)

        :return - a tuple containing mode (str) and the prompt
    """

    prompts = [b'in\:', b'\)#', b'\>', b'\#']

    # determine which 'mode' by prompts
    for i in range(1):
        connObj.write('\r'.encode())
        idx, obj, match = connObj.expect(prompts, timeout=3)

    # print('debug ==>', match)

    if idx == 1:
        mode = 'config'  # conf mode
    elif idx == 2:
        mode = 'priv_1'  # privilege 1
    elif idx == 3:
        mode = 'priv_15'  # privilege 15
    else:
        mode = None
    return mode, match.strip()  # return mode and prompt


def get_ping(connObj, addr='127.0.0.1', prompt=b'>'):
    """ Use PING to determine if an IP address is alive.
        It is first determined if the PINGing device is
        Linux or a network deivce i.e. router/switch.

        :param connObj - Telnet object to use once established with device
        :type connObj - telnetlib connection object
        :param addr - IP address of device to ping
        :type addr - string
        :param prompt - (optional)

        :return - dictionary ['PING':(results)]
    """

    ping_dict = {}

    # determine host system is either network device or linux host
    connObj.write('date'.encode() + '\n\n'.encode())
    _out = connObj.read_until('%'.encode(), timeout=1)

    if '%' in _out.decode():
        # # determine if Arista or Cisco
        # connObj.write('show version | inc rista\n'.encode())   # net device
        # _idx, _obj, _match = connObj.expect([b'Arista', b'EOS'], timeout=2)
        # if _idx == -1:
        #     print('NOTHING FOUND:', _idx, _match)
        # else:
        #     print("GOT IT...Its Arista", _idx)
        # quit()
        # connObj.write('ping '.encode() + addr.encode() + '\n\n'.encode())   # net device
        timeout = 15
        connObj.write('ping '.encode() + addr.encode() +
                      '\n\n'.encode())   # net device
    else:
        connObj.write('ping -W 1 -c 1 '.encode() +
                      addr.encode() + '\n'.encode())   # linux host
        timeout = 2.3

    ping_data = connObj.read_until('packet loss'.encode(), timeout=timeout)
    for line in ping_data.decode().split('\n'):
        if 'packets transmitted' in line:
            ping_dict['PING'] = line
            return ping_dict

    ping_dict['PING'] = ping_data.decode()
    return ping_dict


def get_stats(connObj, key, prompt=b'>'):
    """ Collect basic, common device details.

        :param connObj - Telnet object to use once established with device
        :type connObj - telnetlib connection object

        :return - dictionary of data facts and commands
    """

    print('  - NetMod: {}'.format(prompt))

    stats = {}
    show_cmds = [
        'show ip int brief',
        'show ip route',
        'show vlan',
        'show arp',
        'show vrf',
        'show interface counters',
    ]

    # send commands to device
    connObj.write('term len 0'.encode() + '\n'.encode())
    connObj.read_until(prompt, timeout=1)

    for cmd in show_cmds:
        connObj.write(cmd.encode() + '\n'.encode())
        data = connObj.read_until(prompt, timeout=2)
        stats[cmd] = data.decode()

    return stats


def normalizeData(raw_data, key_val=None):
    """Clean up command generated output"""
    assert type(raw_data) is dict, "normalize data content is invalid type"
    scrub_list = []

    # unpack, scrub, and re-package raw_data
    outp = raw_data[key_val].encode('ascii')
    scrub_list = outp.splitlines()
    return scrub_list


def cfg_mgmt(connObj):
    '''Special function to configure the Management IOU switch'''

    # setup mgmt vlan interface
    connObj.write('int vlan 192'.encode() + '\n\n\n'.encode())
    connObj.write('ip add 192.168.100.20'.encode() +
                  ' 255.255.255.0\n'.encode())
    connObj.write('no shut'.encode() + '\n\n'.encode())

    # setup vlan
    connObj.write('vlan 192'.encode() + '\n\n'.encode())
    connObj.write('name MGMTVLAN'.encode() + '\n\n'.encode())
    connObj.write('exit'.encode() + '\n\n'.encode())

    # setup switchport interfaces
    for i in range(1, 4):
        connObj.write('int e0/'.encode() + str(i).encode() + '\n'.encode())
        connObj.write('switchport access vlan 192'.encode() + '\n\n'.encode())
        connObj.write('no shut'.encode() + '\n\n'.encode())

    for i in range(0, 2):
        connObj.write('int e1/'.encode() + str(i).encode() + '\n'.encode())
        connObj.write('switchport access vlan 192'.encode() + '\n\n'.encode())
        connObj.write('no shut'.encode() + '\n\n'.encode())


def iou_rtr1(connObj, hostname):
    '''Special function to configure IOU router 1 only!'''

    '''
    hostname <host>
    ip domain name <domain>
    crypto key generate rsa
    How many bits in the modulus [512]: 1024/2048
    ip ssh version 2
    !
    aaa new-model
    aaa authentication login default local
    aaa authorization exec default local
    !
    username <config_user> privilege 15 secret <password>
    !
    line vty 0 4
    transport input ssh
    !
    ! for testing only: no authentication on console
    aaa authentication login no_auth none
    line con 0
    privilege level 15
    login authentication no_auth
    '''

    logging.info("hostname {}".format(hostname))
    prompt = '{}(config)#'.format(hostname)  # update prompt
    logging.info("Config prompt {}".format(prompt))

    # if 'fig)#' not in prompt.decode():
    logging.info("Entering config mode")
    connObj.write('conf t\n'.encode())
    connObj.read_until(prompt.encode(), timeout=5)

    # setup mgmt interface
    connObj.write('int eth0/0'.encode() + '\n\n\n'.encode())
    connObj.write('ip add 192.168.1.120'.encode() +
                  ' 255.255.255.0\n'.encode())
    connObj.write('no shut'.encode() + '\n\n'.encode())
    connObj.write('exit'.encode() + '\n\n'.encode())
    connObj.read_until(prompt.encode(), timeout=5)

    # setup ssh configuration
    feedback = [b"modulus [512]:", b"replace them"]
    connObj.write('hostname IOU-1'.encode() + '\n\n\n'.encode())
    connObj.write('ip domain name lab.io'.encode() + '\n\n'.encode())
    connObj.write('crypto key generate rsa'.encode() + '\n'.encode())
    _idx, _obj, _match = connObj.expect(feedback, timeout=5)
    if _idx == 1:
        logging.info("RSA keys already defined. Replacing")
        connObj.write('yes'.encode() + '\n'.encode())
    logging.info("Entering module size of 1024")
    connObj.write('1024'.encode() + '\n'.encode())
    connObj.write('ip ssh version 2'.encode() + '\n\n'.encode())
    connObj.read_until(prompt.encode(), timeout=5)
    connObj.write('aaa new-model'.encode() + '\n'.encode())
    logging.info("Applying AAA configuration")
    connObj.read_until(prompt.encode(), timeout=5)
    connObj.write(
        'aaa authentication login default local'.encode() + '\n\n'.encode())
    connObj.write(
        'aaa authorization exec default local'.encode() + '\n\n'.encode())
    connObj.write(
        'username admin privilege 15 secret Cisco'.encode() + '\n\n'.encode())
    connObj.write('line vty 0 4'.encode() + '\n\n'.encode())
    connObj.write('transport input ssh'.encode() + '\n\n'.encode())
    connObj.read_until(prompt.encode(), timeout=5)
    connObj.write('end'.encode() + '\n'.encode())
    return


def init_lab_dev(host):
    """This function provides the basic init conf

        :param host : the device IP to console into
        :type host : str

        :return : str ('done')
    """
    # dictionary of initial ports and config
    device_init_cfg = {
        '192.168.1.150': {'port': '4000', 'hostname': 'BOGUS', 'mgt_ip': '192.168.1.100', 'eth_ip': '10.10.10.100', 'cfg?': False},
        '192.168.1.100': {'port': '5014', 'hostname': 'ASW-1', 'mgt_ip': '192.168.1.100', 'eth_ip': '10.10.10.100', 'cfg?': True},
        '192.168.1.110': {'port': '5015', 'hostname': 'ASW-2', 'mgt_ip': '192.168.1.110', 'eth_ip': '10.10.10.110', 'cfg?': True},
        '192.168.1.120': {'port': '5013', 'hostname': 'IOU-1', 'mgt_ip': '192.168.1.120', 'eth_ip': '10.10.10.120', 'cfg?': True},
        '192.168.1.10': {'port': '5012', 'hostname': 'IOU-MGMT', 'mgt_ip': '192.168.1.10', 'eth_ip': '', 'cfg?': False},
    }

    # find conf in dict and proceed if cfg flag set
    if host in device_init_cfg:
        print('    Found host IP: {}, configuring'.format(host))

    # check cfg flag, configure devices with initial configuration
    if not device_init_cfg[host]['cfg?']:
        print('    Skipping, cfg flag not set')
        return 'skipping'

    # telnet connection routine
    port = device_init_cfg[host]['port']

    try:
        tn = telnetlib.Telnet('192.168.0.21', port, timeout=2)
    except Exception as e:
        print('    Console connection to port {}, FAILED'.format(port))
        return False
    else:
        print('    Console connection to port {}, PASSED'.format(port))

        mode = get_mode(tn)[0]
        print('    - device mode: {}'.format(mode))

        # login to device, if needed
        if not mode:
            user = 'admin'
            password = 'Cisco'
            print('    - logging into device')
            print('    - username, entered')
            tn.write(user.encode('ascii') + '\n'.encode())
            r = tn.read_until('assword: '.encode(), timeout=2)
            if 'assword' in r.decode():
                tn.write(password.encode('ascii') + '\n'.encode())
            else:
                print('    - password, not required')
        else:
            print('    - Login, not required')

        prompt = get_mode(tn)[1]
        hostname = device_init_cfg[host]['hostname']
        mgmt_ip = device_init_cfg[host]['mgt_ip'] + '/24'
        data_ip = device_init_cfg[host]['eth_ip'] + '/24'

        if mode != 'priv_15':
            tn.write('enable\n'.encode())

        # if 'IOU' in hostname:
        #     print('    - IOU device detected')
        #     iou_rtr1(tn, hostname)
        # raise SystemExit("QUITING: =====")

        if 'IOU' in hostname:
            print('    - IOU device detected')
            iou_rtr1(tn, hostname)
        else:
            tn.write('conf t\n\n\n'.encode())
            tn.write('hostname '.encode() +
                     hostname.encode() + '\n\n\n'.encode())
            tn.write('username admin secret Cisco'.encode() + '\n\n\n'.encode())
            tn.write('int man 1'.encode() + '\n\n\n'.encode())
            tn.write('ip addr '.encode() +
                     mgmt_ip.encode() + '\n\n\n'.encode())
            tn.write('no shut'.encode() + '\n\n'.encode())
            tn.write('int eth 1 '.encode() + '\n\n\n'.encode())
            tn.write('no switchport'.encode() + '\n\n\n'.encode())
            tn.write('ip addr '.encode() +
                     data_ip.encode() + '\n\n\n'.encode())
            tn.write('end\r\n'.encode())
            tn.read_until(prompt, timeout=2)

        # Logout of device
        tn.read_until(prompt, timeout=2)
        tn.write('exit'.encode() + '\n'.encode())
        tn.close()

        return True
