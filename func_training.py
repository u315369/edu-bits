def d1(arg='BLANK'):
    '''This function has a 'optional' passed argument/varible'''
    print("Funct: D1 - This is our intro to functions", arg)


def d2(student, teacher, mfg='Cisco'):
    '''This function has a 'required' passed argument/varible'''
    args = [mfg, student, teacher]

    print("Funct: D2 - This is our intro to functions", mfg)
    print('D2 args:')

    print(' - Mfg ==> ', mfg)
    print(' - Student ==>', student)
    print(' - Teacher ==>', teacher)


def d3(var=[]):
    '''This function has a 'optional' passed list argument/varible'''
    print("--------------------------------------------")
    print("You called function D3")
    print("This is our intro to functions", var)
    print("d3 funct", type(var))


def func1(numerator, denom):
    '''This function has two required arguments/varibles and divides'''
    print("-----------------FUNC 1---------------------------")
    print("Intro to functions using division and returning a value:", numerator, '/', denom)

    dict_result = {'answer': []}

    try:
        result = numerator / denom
        dict_result['answer'].append(result)
    except Exception as e:
        result = 'You got an error::', e

    return dict_result


# d2('Don', 'Tommy', 'Arista')           # required string arg
d2(student='Don', teacher='Tommy', mfg='Arista')           # required string arg
# d2('Arista', 'Don', 'Tommy')           # required string arg

ret = func1(denom=3, numerator=1900)
print('Your answer is:', ret)

for n in range(1, 11):
    ret = func1(denom=n, numerator=62)
    print('Your answer is:', ret)
